package Modelos;

import Conectar.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public abstract class Pessoa {

    protected int id;

    protected String nome;

    protected String endereco;

    protected int idade;

    protected String funcao2;

    public Pessoa() {
    }

    public Pessoa(int id, String nome, String endereco, int idade, String funcao2) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
        this.idade = idade;
        this.funcao2 = funcao2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getFuncao2() {
        return funcao2;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setFuncao2(String funcao2) {
        this.funcao2 = funcao2;
    }

    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_PESSOA2 (id, nome, idade, "
                + "endereco, funcao2) VALUES (?,?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, getId());
            prepareStatement.setString(2, getNome());
            prepareStatement.setInt(3, getIdade());
            prepareStatement.setString(4, getEndereco());
            prepareStatement.setString(5, getFuncao2());

            prepareStatement.executeUpdate();

            System.out.println("Pessoa ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public void pegarUm() {
        String selectSQL = "SELECT * from OO_PESSOA2 WHERE id =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNome(rs.getString("nome"));
                this.setIdade(rs.getInt("idade"));
                this.setEndereco(rs.getString("endereco"));
                this.setFuncao2(rs.getString("funcao2"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    public boolean deleta() {
        Conexao c = new Conexao();
        Connection BD = c.getConexao();
        PreparedStatement ps = null;
        String insertTableSQL = "DELETE FROM OO_PESSOA2 WHERE id = ?";

        try {
            ps = BD.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return true;
    }

    public void atualiza() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE OO_PESSOA2 SET nome =?, idade=?,"
                + " endereco =?, funcao2=? WHERE id =?";
        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(5, this.getId());
            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setString(4, this.getFuncao2());

            prepareStatement.executeUpdate();
            System.out.println("Pessoa ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }
}
