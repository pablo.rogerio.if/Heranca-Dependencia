package Modelos;

import Conectar.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa {

    private String semestre;
    private String curso;

    public Aluno() {
    }

    public Aluno(String semestre, String curso, int id, String nome, String endereco, int idade, String funcao2) {
        super(id, nome, endereco, idade, funcao2);
        this.semestre = semestre;
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public void setFuncao2(String funcao2) {
        this.funcao2 = funcao2;
    }

    @Override
    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_PESSOA2 (id, nome, idade, "
                + "endereco, funcao2, semestre, curso) VALUES (?,?,?,?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, getId());
            prepareStatement.setString(2, getNome());
            prepareStatement.setInt(3, getIdade());
            prepareStatement.setString(4, getEndereco());
            prepareStatement.setString(5, getFuncao2());
            prepareStatement.setString(6, getSemestre());
            prepareStatement.setString(7, getCurso());

            prepareStatement.executeUpdate();

            System.out.println("Aluno ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    @Override
    public void pegarUm() {
        String selectSQL = "SELECT * from OO_PESSOA2 WHERE id =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNome(rs.getString("nome"));
                this.setIdade(rs.getInt("idade"));
                this.setEndereco(rs.getString("endereco"));
                this.setFuncao2(rs.getString("funcao2"));
                this.setSemestre(rs.getString("semestre"));
                this.setCurso(rs.getString("curso"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    @Override
    public void atualiza() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE OO_PESSOA2 SET nome =?, idade=?,"
                + " endereco =?, funcao2=?, semestre=?, curso=? WHERE id =?";
        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(7, this.getId());
            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setString(4, this.getFuncao2());
            prepareStatement.setString(5, this.getSemestre());
            prepareStatement.setString(6, this.getCurso());

            prepareStatement.executeUpdate();
            System.out.println("Aluno atualizado");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

}
