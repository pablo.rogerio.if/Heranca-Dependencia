package Modelos;

import Conectar.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class FuncAdm extends Funcionario {

    private String setor;
    private String funcao;

    public FuncAdm() {
    }

    public FuncAdm(String setor, String funcao, double salario, int id, String nome, String endereco, int idade, String funcao2) {
        super(salario, id, nome, endereco, idade, funcao2);
        this.setor = setor;
        this.funcao = funcao;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    @Override
    public void setFuncao2(String funcao2) {
        this.funcao2 = funcao2;
    }

    @Override
    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        String insertTableSQL = "INSERT INTO OO_PESSOA2 (id, nome, idade, "
                + "endereco, funcao2, funcao, setor, salario) VALUES (?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, getId());
            prepareStatement.setString(2, getNome());
            prepareStatement.setInt(3, getIdade());
            prepareStatement.setString(4, getEndereco());
            prepareStatement.setString(5, getFuncao2());
            prepareStatement.setString(6, getFuncao());
            prepareStatement.setString(7, getSetor());
            prepareStatement.setDouble(8, getSalario());

            prepareStatement.executeUpdate();

            System.out.println("FuncAdm ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    @Override
    public void pegarUm() {
        String selectSQL = "SELECT * from OO_PESSOA2 WHERE id =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try {
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNome(rs.getString("nome"));
                this.setIdade(rs.getInt("idade"));
                this.setEndereco(rs.getString("endereco"));
                this.setFuncao2(rs.getString("funcao2"));
                this.setSalario(rs.getDouble("salario"));
                this.setSetor(rs.getString("setor"));
                this.setFuncao(rs.getString("funcao"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    @Override
    public void atualiza() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        String insertTableSQL = "UPDATE OO_PESSOA2 SET nome =?, idade=?,"
                + " endereco =?, funcao2=?, funcao=?, setor=?, salario=? WHERE id =?";
        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(8, this.getId());
            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setString(4, this.getFuncao2());
            prepareStatement.setString(5, this.getFuncao());
            prepareStatement.setString(6, this.getSetor());
            prepareStatement.setDouble(7, this.getSalario());

            prepareStatement.executeUpdate();
            System.out.println("FuncAdm ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }
}
