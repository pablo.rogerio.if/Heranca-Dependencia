package Modelos;

import Conectar.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Professor extends Funcionario {

    private String disciplina;

    public Professor() {
    }

    public Professor(String disciplina, double salario, int id, String nome, String endereco, int idade, String funcao2) {
        super(salario, id, nome, endereco, idade, funcao2);
        this.disciplina = disciplina;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public void setFuncao2(String funcao2) {
        this.funcao2 = funcao2;
    }

    @Override
    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_PESSOA2 (id, nome, idade, "
                + "endereco, funcao2, salario, disciplina) VALUES (?,?,?,?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, getId());
            prepareStatement.setString(2, getNome());
            prepareStatement.setInt(3, getIdade());
            prepareStatement.setString(4, getEndereco());
            prepareStatement.setString(5, getFuncao2());
            prepareStatement.setDouble(6, getSalario());
            prepareStatement.setString(7, getDisciplina());

            prepareStatement.executeUpdate();

            System.out.println("Professor ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    @Override
    public void pegarUm() {
        String selectSQL = "SELECT * from OO_PESSOA2 WHERE id =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNome(rs.getString("nome"));
                this.setIdade(rs.getInt("idade"));
                this.setEndereco(rs.getString("endereco"));
                this.setFuncao2(rs.getString("funcao2"));
                this.setSalario(rs.getDouble("salario"));
                this.setDisciplina(rs.getString("disciplina"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    @Override
    public void atualiza() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE OO_PESSOA2 SET nome =?, idade=?,"
                + " endereco =?, funcao2=?, salario=?, disciplina=? WHERE id =?";
        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(7, this.getId());
            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setString(4, this.getFuncao2());
            prepareStatement.setDouble(5, this.getSalario());
            prepareStatement.setString(6, this.getDisciplina());

            prepareStatement.executeUpdate();
            System.out.println("Professor ok");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }
}
