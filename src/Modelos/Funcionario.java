/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Aluno
 */
public abstract class Funcionario extends Pessoa {

    protected double salario;
    public Funcionario(){}
    
    public Funcionario(double salario, int id, String nome, String endereco, int idade, String funcao2) {
        super(id, nome, endereco, idade, funcao2);
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

}
